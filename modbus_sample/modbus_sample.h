#pragma once

#include <QtWidgets/QMainWindow>
#include <QEvent>
#include "ui_modbus_sample.h"

#include "modbus.h"

class modbus_sample : public QMainWindow
{
    Q_OBJECT

public:
    modbus_sample(QWidget *parent = Q_NULLPTR);
	~modbus_sample();
private:
    Ui::modbus_sampleClass ui;
	int refresh_timerid;

//##�ŧimodbus context�ܼ�###
	modbus_t *md_ctx;

	uint8_t    md_input[8]; // input bit
	uint16_t   md_input_reg[8]; // input word(2byte 0-65535)
	

protected:
	virtual void timerEvent(QTimerEvent *event);

	private Q_SLOTS:

	void start_connect();
	void refresh();
	void write_coil();
	void write_register();
};
