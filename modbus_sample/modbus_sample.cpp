#include "modbus_sample.h"
#include <QDebug>

modbus_sample::modbus_sample(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	md_ctx = NULL;


	memset(md_input, 0, sizeof(md_input));
	memset(md_input_reg, 0, sizeof(md_input_reg));


	ui.reg_val_->setValidator(new QIntValidator(0, 65535, this));
	ui.modbus_port->setValidator(new QIntValidator(0, 65535, this));

	connect(ui.mb_conn_btn, SIGNAL(released()), SLOT(start_connect()));
	connect(ui.coil_w_btn, SIGNAL(released()), SLOT(write_coil()));
	connect(ui.reg_w_btn, SIGNAL(released()), SLOT(write_register()));

	//start_connect();
}

modbus_sample::~modbus_sample()
{
	killTimer(refresh_timerid);
	if (md_ctx) {
		modbus_close(md_ctx);
		md_ctx = NULL;
	}
}

void modbus_sample::start_connect() {
	if (md_ctx == NULL) {
		QString ip = ui.modbus_ip->text();
		int port = ui.modbus_port->text().toInt();
		//### modbus 建立TCP 連線
		md_ctx = modbus_new_tcp(ip.toStdString().c_str(), port);

		if (modbus_connect(md_ctx) == -1) {
			modbus_close(md_ctx);
			md_ctx = NULL;
			qDebug() << "modbus connection error";
			return;
		}
		ui.mb_conn_btn->setText("Disconnect");
		refresh_timerid = startTimer(100);
	}
	else {
		killTimer(refresh_timerid);
		if (md_ctx) {
			modbus_close(md_ctx);
			md_ctx = NULL;
		}
		ui.mb_conn_btn->setText("Connect");
	}
}



void modbus_sample::timerEvent(QTimerEvent * event)
{
	if (event->timerId() == refresh_timerid) {
		refresh();
	}
}

void modbus_sample::refresh()
{
	if (md_ctx == NULL) return;

	//## 透過modbus function code 讀取設備數值

	//## 使用Code 02 讀取input，資料型態 0-1
	if (modbus_read_input_bits(md_ctx, 0, 8, md_input) != -1) {
		for (int i = 0; i < 8; ++i) {
			QTableWidgetItem *item = ui.tableWidget->item(0, i);
			if (!item) {
				item = new QTableWidgetItem();
				ui.tableWidget->setItem(0, i, item);
			}
			item->setText(QString::number(md_input[i]));
		}

	}
	//## 使用Code 04 讀取 input register，資料型態 0-65535
	if (modbus_read_input_registers(md_ctx, 0, 8, md_input_reg) != -1) {
		QString in_to_string = QString("input register: ");
		for (int i = 0; i < 8; ++i) {
			QTableWidgetItem *item = ui.tableWidget->item(1, i);
			if (!item) {
				item = new QTableWidgetItem();
				ui.tableWidget->setItem(1, i, item);
			}
			item->setText(QString::number(md_input_reg[i]));
		}
	}
}

void modbus_sample::write_coil()
{
	int start_addr = ui.coil_start_addr_combo->currentText().toInt();
	int status = ui.coil_status_combo->currentText().toInt();
	//## 使用Code 05 寫入單一Coil
	if (modbus_write_bit(md_ctx, start_addr, status) != -1) {
		qDebug() << "write coil success";
	}

}

void modbus_sample::write_register()
{
	int start_addr = ui.reg_start_addr_combo->currentText().toInt();
	int val = ui.reg_val_->text().toInt();
	//## 使用Code 06 寫入單一hoding register
	if (modbus_write_register(md_ctx, start_addr, val) != -1) {
		qDebug() << "write hoding register success";
	}
}
